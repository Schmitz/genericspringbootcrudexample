# SpringBoot and Hazelcast
[ServiceInterface]: src/etc/readme-img/ServiceInterface.png "ServiceInterface.png"
[InteliJAppStart]: src/etc/readme-img/InteliJAppStart.png "InteliJAppStart.png"
[InteliJAppStartMenu]: src/etc/readme-img/InteliJAppStartMenu.png "InteliJAppStartMenu.png"
[AddConfig]: src/etc/readme-img/AddConfig.png "AddConfig.png"
[AddConfigPopUp]: src/etc/readme-img/AddConfigPopUp.png "AddConfigPopUp.png"
[ControllerDiagram]: src/etc/readme-img/ControllerDiagram.png "ControllerDiagram.png"
[RestServiceDiagram]: src/etc/readme-img/RestServiceDiagram.png "RestServiceDiagram.png"
[ArchitectureOverview]: src/etc/readme-img/ArchitectureOverview.png "ArchitectureOverview.png"
[ExampleNoteRequest]: src/etc/readme-img/ExampleNoteRequest.png "ExampleNoteRequest.png"
[PortfolioItemsRequest]: src/etc/readme-img/PortfolioItemsRequest.png "PortfolioItemsRequest.png"

This Application uses a mariadb Database 
including the modern SpringBoot Techniques and using a HazelCast Cluster Server.
<br /><br />
<strong>Attention: </strong> All new Files are well documented. see /etc/java-doc for the java Documentation.<br />
<strong>It is on you to take care that everything which is added is documented as well too!</strong> 
- - - - 
## SetUp
The Application comes with a embedded tomcat.<br />
see https://www.baeldung.com/spring-boot-war-tomcat-deploy<br /><br />
do the settings in: application-{profile}.properties
<br />
application-dev.properties for your local setup<br /><br />
<strong>Check Database Settings.</strong><br />
You can import the intial_dump from:<br />
`/src/etc/database/initial-dump.sql.zip`<br />
into a new Database:<br /> `create database db`<br /><br />
or use an existing `bwpws` Database.
<br /><br />
<strong>Take care about this port property</strong><br />
`server.port=0`<br />

this can be used to run multiple servers on the local machine to test the cluster caching. The Tomcat picks up a random port with this setting.<br />
Usually `server.port=8080` is set.<br />
<br />
Assuming you have checked the database settings. <br />
run `mvn clean package -P dev` to build the Application. Using `mvn clean compile` sometimes did not 
write the application.properties so `package` is safe.

now open the file `/src/main/java/de/get2web/Application.java` and run this class.

InteliJ provides a start button :-)<br />
![Image of InteliJAppStart][InteliJAppStart] 
![Image of InteliJAppStartMenu][InteliJAppStartMenu] 

After hopefully everything starts up correctly or after some TroubleShooting you should see this in your console:<br />
```
Tomcat started on port(s): 58763 (http) with context path ''
Started Application in 15.432 seconds (JVM running for 19.983)
```
here you can see the port if you have a random one.

now you can open:<br />
http://localhost:port/swagger-ui.html

### Running multiple Instances
Assuming you are using InteliJ just add a new Run/Debug Setting see Screenshots:

![Image of AddConfig][AddConfig] <br />
![Image of AddConfigPopUp][AddConfigPopUp] 

- - - -
## Architecture
Section describes the Software Design Patterns. Beginning from the Top to Bottom
#### DataObjects
We have two kinds of Objects which should be mentioned:<br /><br />
<strong>DTO (DataTransportObjects)</strong><br />
Objects are used for the JSON Data Exchange with the clients<br /><br />
<strong>Entity</strong><br />
The Database Entity Objects.<br />
 - - - - 
### Architecture Overview
In the separate main packages are also ReadMe.md files which describes the structure in the package.
Each main package stands for an own module which brings a service layer for an interaction with this package.
So each of the packages can be understood as a module and they could also be separated into own modules. 
So a FinanceData Module would be reusable in other projects for example.

<strong>Basic Rules</strong>
- No module specific calls outside the module 
    - This means: No Database Access outside the datxabase module. each other modules 
must use the database service from the database module. 
- Data Transformation happens where it is needed. 
     - This means: The RestLayer transform a DatabaseObject to a DTO. 
The Database Service returns Database-Objects. 
So those can be used in several other services they can transform their own
- HazelCast Caching happens in the modules. so we automatically have a multi layer caching. the database cache for example evicts on changes. 
Higher level caches could have a different setting.
  

The following Diagram should give a quick Overview about the Structure of the Application.<br />
![Image of ArchitectureOverview][ArchitectureOverview]<br /><br />
You can see that Database, FinanceData etc. are own modules which can use each others. So also the RestService can get Objects from any Service.<br /><br />

So a typical REST request for a direct Database Object and a Porfolio Object looks like in the following two Diagrams<br />
![Image of ExampleNoteRequest][ExampleNoteRequest]
![Image of PortfolioItemsRequest][PortfolioItemsRequest]
<br /><br />
In the current case i see one Database Service Impl. Maybe in future this can be separated into smaller packages.
There could be a Config MicroService, a Portfolio Microservice. But all this would take a longer time then to bring first the actual 
Implementation in a more structured Software Design ... beginning from that Base makes it easier to divide this later into
microservice architecture. 
- - - -

 
## HowTo: Create a new Domain Object from Rest to DB
The following Files must be created. (DomainObject) stands for the name of Your Object like Transaction for Example
```
|--database
|  |--service
|     |--{DomainObject}EntityService implements CrudService<T>
|  |--persistence
|     |--database
|     |  |--enttiy
|     |     |--{DomainObject}Entity.java *the Entity Object if not exists
|     |--repository
|        |--database
|        |  |--{DomainObject}EntityJpaRepository.java *see exisiting Repository as example
|        |--hazelcast
|        |  |--{DomainObject}EntityRepository.java *see exisiting Repository as example
|        |--custom **OPTIONAL**
|           |--{DomainObject}CustomRepository.java for custom Implementations if needed  
|--rest **If Database Object is need in the RestLayer**
   |--v1
      |--controller
      |  |--{DomainObject}RestController.java *see exisiting Controller as example
      |--mapper
      |  |--{DomainObject}Mapper.java *see exisiting Mapper as example
      |--model
      |  |--{DomainObject}DTO.java *see exisiting Model as example
      |--service
      |  |--{DomainObject}RestService.java *see exisiting Service as example


```

## Dependencies
__Name__ - __Version__<br />
- SpringBoot - 2.0.4.RELEASE
    - spring-boot-starter-web
    - spring-boot-starter-tomcat
    - spring-boot-starter-jdbc
    - spring-boot-starter-data-jpa
- modelmapper - 2.3.0
- hazelcast - 3.11
    - hazelcast-spring
    - hazelcast-wm - 3.8.3
    - spring-data-hazelcast - 2.1
- swagger - 2.7.0
    - springfox-swagger2
    - springfox-swagger-ui
- mysql-connector-java - 5.1.38
          

## Useful links
https://hazelcast.com/<br />
https://spring.io/projects/spring-boot<br />
http://modelmapper.org/

The .plantuml files are generated with the InteliJ Plugin SketchIt. The can be shown with the plantuml Plugin. 
see this for TroubleShooting: http://plantuml.com/graphviz-dot
- - - - 
recommended to clone: https://github.com/hazelcast/hazelcast-code-samples<br />
here are several different implementation examples of hazelcast
- - - -
```
Thanks for reading and the good Time
   _____                .__                             .__                          
  /  _  \ ___  ___ ____ |  |   __  _  _______    ______ |  |__   ___________   ____  
 /  /_\  \\  \/  // __ \|  |   \ \/ \/ /\__  \  /  ___/ |  |  \_/ __ \_  __ \_/ __ \ 
/    |    \>    <\  ___/|  |__  \     /  / __ \_\___ \  |   Y  \  ___/|  | \/\  ___/ 
\____|__  /__/\_ \\___  >____/   \/\_/  (____  /____  > |___|  /\___  >__|    \___  >
        \/      \/    \/                     \/     \/       \/     \/            \/ 

```
