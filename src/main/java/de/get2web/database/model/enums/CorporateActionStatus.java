package de.get2web.database.model.enums;

public enum CorporateActionStatus {

    EXECUTED,
    ROLLEDBACK

}
