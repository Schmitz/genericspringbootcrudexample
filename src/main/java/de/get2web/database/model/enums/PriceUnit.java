package de.get2web.database.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * see TransactionType
 */
public enum PriceUnit {
    CURRENCY,
    PERCENTAGE,
    POINT;

    /**
     * better for usage in the rest client to exchange the ordinal
     *
     * @return
     */
    @JsonValue
    public int toValue() {
        return ordinal();
    }
}
