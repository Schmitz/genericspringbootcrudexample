/**
 * Enums copied from bwpws but packaged in one place. They are used for Settings in the database so they are member of the database package
 */
package de.get2web.database.model.enums;