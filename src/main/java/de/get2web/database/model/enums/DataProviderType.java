package de.get2web.database.model.enums;

public enum DataProviderType {

    MDG,
    ARIVA,
    ONVMDG
}
