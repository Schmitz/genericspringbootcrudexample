package de.get2web.database.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Transaction type information.
 * <p>
 * ENUMS should define the ordinal to be safe also when something is changing. this is also recommended to exchange with the rest client.
 * I added the JSONValue to get this working for the fe
 * <p>
 * example:
 * <p>
 * <p>
 * TransactionType {
 * BUY(0),
 * SELL(1);
 * ...
 * <p>
 * private final int val;
 * <p>
 * TransactionType(int v) {
 * val = v;
 * }
 * <p>
 * public int getVal() {
 * return val;
 * }
 * }
 */
public enum TransactionType {
    BUY,
    SELL,
    INTEREST_PAYMENT,
    CHANGE_ORDER,
    CANCEL_ORDER,
    CORPORATE_ACTION_BONUS_STOCK,
    CORPORATE_ACTION_DIVIDEND,
    CORPORATE_ACTION_PAYOUT,
    CORPORATE_ACTION_SPLIT,
    CORPORATE_ACTION_COUPON,
    ADMIN_ACCOUNTING,
    CORPORATE_ACTION_PREEMPTIVE_RIGHT,
    CORPORATE_ACTION_SPLIT_ISIN_CHANGE,
    CREDIT_ACCOUNT_DEBIT,
    CREDIT_ACCOUNT_CREDIT,
    PAYMENT_IN_OUT,
    DEPOT_COST_DEBIT,
    PAYMENT,
    WITHDRAWAL,
    INITIAL_CASH;

    /**
     * better for usage in the rest client to exchange the ordinal
     *
     * @return
     */
    @JsonValue
    public int toValue() {
        return ordinal();
    }
}
