package de.get2web.database.model.enums;


public enum CorporateActionType {

    DIVIDEND,
    PAYOUT,
    SPLIT,
    COUPON,
    BONUS,
    PREEMPTIVERIGHT,
    SPLIT_ISIN_CHANGE
}
