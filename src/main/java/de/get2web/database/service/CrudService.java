package de.get2web.database.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * <h1>Class CrudService</h1>
 * <p>
 * a generic CrudService interface to have the CRUD Operations in its implementations.<br />
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-30
 * @since 1.0
 */
public interface CrudService<T> {

    /**
     * <h3>save(T t)</h3>
     * <p>save a Entity Object</p>-
     *
     * @param t T Database Entity Object
     * @return T Database Entity Object
     */
    T save(T t);

    /**
     * <h3>getById(Long id)</h3>
     * <p>Get one Database Object by Id</p>
     *
     * @param id Long id
     * @return
     */
    Optional<T> getById(Long id);

    /**
     * <h3>list(Pageable pageable)</h3>
     * <p>get all Database Objects with a Pageable</p>
     *
     * @param pageable {@link Pageable}
     * @return Page<T> {@link Page}
     */
    Page<T> list(Pageable pageable);

    /**
     * <h3>listWithId(Long id, Pageable pageable)</h3>
     * <p>
     * for data objects where we have more than one entry with an id
     * for example: all transactions for one order id
     * </p>
     *
     * @param id       Long
     * @param pageable {@link Pageable}
     * @return Page<T> {@link Page}
     */
    Page<T> listWithId(Long id, Pageable pageable);

    /**
     * <h3>delete(Long id)</h3>
     * <p>deletes a db entrie by id</p>
     *
     * @param id Long id
     * @return Boolean
     */
    Boolean delete(Long id);

}
