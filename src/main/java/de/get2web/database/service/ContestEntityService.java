package de.get2web.database.service;

import de.get2web.database.persistence.database.entity.ContestEntity;
import de.get2web.database.persistence.database.repository.hazelcast.ContestEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * <h1>Class ContestService</h1>
 * <p>CRUD Service for the {@link ContestEntity} Object. Uses Hazelcast Cache 'EntityServiceCache'</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-30
 * @since 1.0
 */
@Service
@CacheConfig(cacheNames = "EntityServiceCache")
public class ContestEntityService implements CrudService<ContestEntity> {


    @Autowired
    private ContestEntityRepository repository;


    /**
     * <h2>save(ContestEntity contestEntity)</h2>
     * <p>saves the {@link ContestEntity} object. evicts the cache</p>
     * {@inheritDoc}
     *
     * @param contestEntity {@link ContestEntity}
     * @return {@link ContestEntity}
     */
    @Override
    @CacheEvict(value = "ContestEntity", allEntries = true)
    public ContestEntity save(ContestEntity contestEntity) {
        return this.repository.save(contestEntity);
    }

    /**
     * <h2>getById(Long id)</h2>
     * <p>get {@link Optional} from {@link ContestEntity} cached in ContestEntity cache</p>
     * {@inheritDoc}
     *
     * @param id Long id
     * @return {@link Optional} from {@link ContestEntity}
     */
    @Override
    @Cacheable(value = "ContestEntity")
    public Optional<ContestEntity> getById(Long id) {
        return this.repository.findById(id);
    }

    /**
     * <h2>list(Pageable pageable)</h2>
     * <p>loads a Page<ContestEntity> from the repository</p>
     * {@inheritDoc}
     *
     * @param pageable {@link Pageable}
     * @return {@link Page} of {@link ContestEntity}
     */
    @Override
    @Cacheable(value = "ContestEntity")
    public Page<ContestEntity> list(Pageable pageable) {
        return this.repository.findAll(pageable);
    }

    /**
     * <h2>list(Long id, Pageable pageable)</h2>
     * <p>loads a Page<ContestEntity> from the repository
     * with he given tenantId</p>
     * {@inheritDoc}
     *
     * @param id       Long id
     * @param pageable {@link Pageable}
     * @return {@link Page} of {@link ContestEntity}
     */
    @Override
    @Cacheable(value = "ContestEntity")
    public Page<ContestEntity> listWithId(Long id, Pageable pageable) {
        return this.repository.findByTenantId(id, pageable);
    }

    /**
     * <h2>delete(Long id)</h2>
     * <p>evicts the cache</p>
     * {@inheritDoc}
     *
     * @param id Long id
     * @return
     */
    @Override
    @CacheEvict(value = "ContestEntity", allEntries = true)
    public Boolean delete(Long id) {
        try {
            this.repository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
