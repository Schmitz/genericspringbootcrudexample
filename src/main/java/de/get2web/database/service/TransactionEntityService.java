package de.get2web.database.service;

import de.get2web.database.persistence.database.entity.TransactionEntity;
import de.get2web.database.persistence.database.repository.hazelcast.TransactionEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * <h1>Class TransactionService</h1>
 * <p>CRUD Service for the {@link TransactionEntity} Object. Uses Hazelcast Cache 'EntityServiceCache'</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-30
 * @since 1.0
 */
@Service
@CacheConfig(cacheNames = "EntityServiceCache")
public class TransactionEntityService implements CrudService<TransactionEntity> {


    @Autowired
    private TransactionEntityRepository repository;

    /**
     * <h2>save(TransactionEntity transactionEntity)</h2>
     * <p>saves the {@link TransactionEntity} object. evicts the cache</p>
     * {@inheritDoc}
     *
     * @param contestEntity {@link TransactionEntity}
     * @return {@link TransactionEntity}
     */
    @Override
    @CacheEvict(value = "TransactionEntity")
    public TransactionEntity save(TransactionEntity transactionEntity) {
        return this.repository.save(transactionEntity);
    }

    /**
     * <h2>getById(Long id)</h2>
     * <p>get {@link Optional} from {@link TransactionEntity} cached in TransactionEntity cache</p>
     * {@inheritDoc}
     *
     * @param id Long id
     * @return {@link Optional} from {@link TransactionEntity}
     */
    @Override
    @Cacheable("TransactionEntity")
    public Optional<TransactionEntity> getById(Long id) {
        return this.repository.findById(id);
    }

    /**
     * <h2>list(Pageable pageable)</h2>
     * <p>loads a Page<TransactionEntity> from the repository</p>
     * {@inheritDoc}
     *
     * @param pageable {@link Pageable}
     * @return {@link Page} of {@link TransactionEntity}
     */
    @Override
    @Cacheable("TransactionEntity")
    public Page<TransactionEntity> list(Pageable pageable) {
        return this.repository.findAll(pageable);
    }

    /**
     * <h2>list(Long id, Pageable pageable)</h2>
     * <p>not implemented yet</p>
     * {@inheritDoc}
     *
     * @param id       Long id
     * @param pageable {@link Pageable}
     * @return {@link Page} of {@link TransactionEntity}
     */
    @Override
    public Page<TransactionEntity> listWithId(Long id, Pageable pageable) {
        return null;
    }

    /**
     * <h2>delete(Long id)</h2>
     * <p>evicts the cache</p>
     * {@inheritDoc}
     *
     * @param id Long id
     * @return
     */
    @Override
    @CacheEvict(value = "TransactionEntity")
    public Boolean delete(Long id) {
        try {
            this.repository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
