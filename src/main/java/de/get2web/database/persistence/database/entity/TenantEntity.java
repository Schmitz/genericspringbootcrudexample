package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Tenant information entity.
 */
@Entity
@Table(name = "bw_tenant")
public class TenantEntity implements Serializable {

    private static final long serialVersionUID = -7492686379690499240L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Tenant name.
     */
    private String name;

    /**
     * Short description.
     */
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
