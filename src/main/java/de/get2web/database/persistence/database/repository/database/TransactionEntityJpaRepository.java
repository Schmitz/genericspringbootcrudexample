package de.get2web.database.persistence.database.repository.database;

import de.get2web.database.persistence.database.entity.TransactionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Class TransactionEntityJpaRepository
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO aschmitz describe Class
 * @created 2018-12-04
 * @since 1.0
 */

public interface TransactionEntityJpaRepository extends PagingAndSortingRepository<TransactionEntity, Long> {

    /**
     * Query is for the hazelcast server
     *
     * @return
     */
    @Query("SELECT t.id FROM TransactionEntity t")
    Iterable<Integer> findAllId();

}
