package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * User portfoli/watchlist data.
 */
@Entity
@Table(name = "bw_portfolio")
public class PortfolioEntity implements Serializable {

    private static final long serialVersionUID = -1321535522584262469L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Name data.
     */
    private String name;

    /**
     * Starting capital data.
     */
    private Double valueAtActivationDate;

    /**
     * ISSO currency key.
     */
    private String isoCurrency;

    /**
     * Activation date time.
     */
    private LocalDateTime activationDate;

    /**
     * Watchlist flag.
     * - true	=>	watchlist
     * - false	=>	portfolio.
     */
    private Boolean watchlist;

    /**
     * Related {@link UserEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    /**
     * Current capital on disposal.
     */
    private Double credit;

    /**
     * Calculate using latest price or buy exchange price
     */
    private Boolean useLatestPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValueAtActivationDate() {
        return valueAtActivationDate;
    }

    public void setValueAtActivationDate(Double valueAtActivationDate) {
        this.valueAtActivationDate = valueAtActivationDate;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public LocalDateTime getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(LocalDateTime activationDate) {
        this.activationDate = activationDate;
    }

    public Boolean getWatchlist() {
        return watchlist;
    }

    public void setWatchlist(Boolean watchlist) {
        this.watchlist = watchlist;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Boolean getUseLatestPrice() {
        return useLatestPrice;
    }

    public void setUseLatestPrice(Boolean latestPrice) {
        this.useLatestPrice = latestPrice;
    }

    @Override
    public int hashCode() {
        final int prime = 97;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PortfolioEntity other = (PortfolioEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
