package de.get2web.database.persistence.database.entity;

import javax.persistence.*;


/*
 * <wertpapier>
          <ag>4</ag>
          <isin>DE0005167902</isin>
          <wkn>516790</wkn>
          <name>3U Holding</name>
          <type>Aktie</type>
          <branche>Telekommunikation</branche>
          <herkunft>Deutschland</herkunft>
          <land>Deutschland</land>
          <subgattung>Inhaberaktie</subgattung>
     </wertpapier>
 */
@Entity
@Table(name = "ariva_instruments")
public class ArivaInstrument {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String ag;

    private String isin;

    private String wkn;

    private String name;

    private String type;

    private String branch;

    private String origin;

    private String country;

    private String subtype;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAg() {
        return ag;
    }

    public void setAg(String ag) {
        this.ag = ag;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getWkn() {
        return wkn;
    }

    public void setWkn(String wkn) {
        this.wkn = wkn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    @Override
    public String toString() {
        return "ArivaInstrument [id=" + id + ", type=" + type + ", ag=" + ag + ", isin=" + isin + ", wkn=" + wkn + ", name=" + name
                + ", branch=" + branch + ", origin=" + origin + ", country=" + country + ", subtype="
                + subtype + "]";
    }

}
