package de.get2web.database.persistence.database.entity;

/**
 * Limit type data information
 * - TOP limit,
 * - BOTTOM limit.
 */
public enum LimitType {

    TOP, BOTTOM, TOP_BID, BOTTOM_BID, TOP_ASK, BOTTOM_ASK

}
