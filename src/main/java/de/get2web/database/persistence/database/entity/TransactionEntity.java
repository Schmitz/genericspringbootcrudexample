package de.get2web.database.persistence.database.entity;


import de.get2web.database.model.enums.PriceUnit;
import de.get2web.database.model.enums.TransactionType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Transactions entity.
 */
@Entity
@Table(name = "bw_transactions")
public class TransactionEntity implements Serializable {

    private static final long serialVersionUID = 2686664146346184715L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link PortfolioEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "portfolio_id")
    private PortfolioEntity portfolio;

    /**
     * Related {@link InstrumentEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "instrument_id")
    private InstrumentEntity instrument;

    /**
     * Quote Code Exchange ID, {@link Quote} idExchange.
     */
    private String idExchangeExternal;

    /**
     * Related {@link CorporateActionEntity}.
     */
    private CorporateActionEntity corporateAction;

    /**
     * {@link TransactionType}.
     */
    @Enumerated(EnumType.STRING)
    private TransactionType transactionType;

    /**
     * Transaction execution date time.
     */
    private LocalDateTime executionDate = LocalDateTime.now();

    /**
     * ISSO currency code.
     */
    private String isoCurrency = "EUR";

    /**
     * Exchange rate value.
     */
    private Double exchangeRate = 1.0;

    /**
     * Exchange rate date time when compared.
     */
    private LocalDateTime exchangeRateDate = LocalDateTime.now();

    /**
     * {@link PriceUnit}.
     */
    @Enumerated(EnumType.STRING)
    private PriceUnit priceUnit = PriceUnit.CURRENCY;

    /**
     * Instrument quantity in this transaction.
     */
    private Double quantity;

    /**
     * ISO 8601 date format of the last transaction process date.
     */
    private LocalDateTime timestamp = LocalDateTime.now();

    /**
     * Buy or sell price value.
     */
    private Double price;

    /**
     * Price date when requested, date time value.
     */
    private LocalDateTime priceDate;

    /**
     * Commission.
     */
    private Double transactionCost;

    /**
     * Accrued interest in the case of Bonds
     */
    private Double accruedInterest;

    /**
     * Code contributor of the quote (given by the data provider)
     */
    private String codeContributor;

    /**
     * Code exchange of the quote (given by the data provider)
     */
    private String codeExchange;

    /**
     * Editable or not editable flag.
     */
    private Boolean editable = true;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PortfolioEntity getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(PortfolioEntity portfolio) {
        this.portfolio = portfolio;
    }

    public InstrumentEntity getInstrument() {
        return instrument;
    }

    public void setInstrument(InstrumentEntity instrument) {
        this.instrument = instrument;
    }

    public CorporateActionEntity getCorporateAction() {
        return corporateAction;
    }

    public void setCorporateAction(CorporateActionEntity corporateAction) {
        this.corporateAction = corporateAction;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public LocalDateTime getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(LocalDateTime executionDate) {
        this.executionDate = executionDate;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public LocalDateTime getExchangeRateDate() {
        return exchangeRateDate;
    }

    public void setExchangeRateDate(LocalDateTime exchangeRateDate) {
        this.exchangeRateDate = exchangeRateDate;
    }

    public PriceUnit getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(PriceUnit priceUnit) {
        this.priceUnit = priceUnit;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDateTime getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(LocalDateTime priceDate) {
        this.priceDate = priceDate;
    }

    public String getIdExchangeExternal() {
        return idExchangeExternal;
    }

    public void setIdExchangeExternal(String idExchangeExternal) {
        this.idExchangeExternal = idExchangeExternal;
    }

    public Double getTransactionCost() {
        return transactionCost;
    }

    public void setTransactionCost(Double transactionCost) {
        this.transactionCost = transactionCost;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }

    public Double getAccruedInterest() {
        return accruedInterest;
    }

    public void setAccruedInterest(Double accruedInterest) {
        this.accruedInterest = accruedInterest;
    }

    public String getCodeContributor() {
        return codeContributor;
    }

    public void setCodeContributor(String codeContributor) {
        this.codeContributor = codeContributor;
    }

    public String getCodeExchange() {
        return codeExchange;
    }

    public void setCodeExchange(String codeExchange) {
        this.codeExchange = codeExchange;
    }

    @Override
    public int hashCode() {
        final int prime = 231;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TransactionEntity other = (TransactionEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
