package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Exchange rate history data. This is used for comparison.
 */
@Entity
@Table(name = "bw_exchange_rate")
public class ExchangeRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link ContestEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "contest_id")
    private ContestEntity contest;

    /**
     * Third party exchange id.
     */
    private String idExternal;

    /**
     * ISSO currency key, for conversion from.
     */
    @Column(length = 3)
    private String isoCurrencyFrom;

    /**
     * ISSO currency key, for conversion to.
     */
    @Column(length = 3)
    private String isoCurrencyTo;

    /**
     * Exchange rate value.
     */
    private Double exchangeRate;

    /**
     * Date time data.
     */
    private LocalDate date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContestEntity getContest() {
        return contest;
    }

    public void setContest(ContestEntity contest) {
        this.contest = contest;
    }

    public String getIdExternal() {
        return idExternal;
    }

    public void setIdExternal(String idExternal) {
        this.idExternal = idExternal;
    }

    public String getIsoCurrencyFrom() {
        return isoCurrencyFrom;
    }

    public void setIsoCurrencyFrom(String isoCurrencyFrom) {
        this.isoCurrencyFrom = isoCurrencyFrom;
    }

    public String getIsoCurrencyTo() {
        return isoCurrencyTo;
    }

    public void setIsoCurrencyTo(String isoCurrencyTo) {
        this.isoCurrencyTo = isoCurrencyTo;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
