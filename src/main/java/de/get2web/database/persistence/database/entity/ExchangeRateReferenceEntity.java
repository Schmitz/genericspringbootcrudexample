package de.get2web.database.persistence.database.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * Exchange rate data provider internal data.
 */
@Entity
@Table(name = "bw_exchange_rate_reference")
public class ExchangeRateReferenceEntity implements Serializable {

    private static final long serialVersionUID = 2892109482184176341L;

    /**
     * {@link ExchangeRateReferencePK}
     */
    @EmbeddedId
    private ExchangeRateReferencePK id;

    /**
     * Data provider exchange external id.
     */
    private String idExternal;

    public ExchangeRateReferenceEntity() {
        super();
    }

    public ExchangeRateReferenceEntity(String isoCurrencyFrom, String isoCurrencyTo, ContestEntity contest) {
        super();
        this.id = new ExchangeRateReferencePK(isoCurrencyFrom, isoCurrencyTo, contest);
    }

    public ExchangeRateReferenceEntity(String isoCurrencyFrom, String isoCurrencyTo, ContestEntity contest, String idExternal) {
        super();
        this.id = new ExchangeRateReferencePK(isoCurrencyFrom, isoCurrencyTo, contest);
        this.idExternal = idExternal;
    }

    public String getIdExternal() {
        return idExternal;
    }

    public void setIdExternal(String idExternal) {
        this.idExternal = idExternal;
    }

    public ExchangeRateReferencePK getId() {
        return id;
    }

    public void setId(ExchangeRateReferencePK id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 17;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExchangeRateReferenceEntity other = (ExchangeRateReferenceEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
