package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * {@link InstrumentEntity} price data information.
 * Calculated from current world wide Instrument performance (quotes from third party data provider).
 */
@Entity
@Table(name = "bw_price")
public class PriceEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link ContestEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "contest_id")
    private ContestEntity contest;

    /**
     * Related {@link InstrumentEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "instrument_id")
    private InstrumentEntity instrument;

    /**
     * Calculation date time.
     */
    private LocalDate date;

    /**
     * Current price value.
     */
    private Double price;

    /**
     * Open price value.
     */
    private Double open;

    /**
     * Highest price value.
     */
    private Double high;

    /**
     * Lowest price value.
     */
    private Double low;

    /**
     * Instrument volume or quantity.
     */
    private Double volume;

    /**
     * Total Instrument volume, available for trading.
     */
    private Double totalVolume;

    /**
     * Price date time, provided from third party data provider.
     */
    private LocalDateTime priceDate;

    /**
     * Previous price value.
     */
    private Double previousPrice;

    /**
     * Previous price date time calculated (from data provider).
     */
    private LocalDateTime previousPriceDate;

    /**
     * Quote Relative difference, {@link Quote} diffRel.
     */
    private Double diffRel;

    /**
     * Quote Absolute difference, {@link Quote} diffAbs.
     */
    private Double diffAbs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContestEntity getContest() {
        return contest;
    }

    public void setContest(ContestEntity contest) {
        this.contest = contest;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getOpen() {
        return open;
    }

    public void setOpen(Double open) {
        this.open = open;
    }

    public Double getHigh() {
        return high;
    }

    public void setHigh(Double high) {
        this.high = high;
    }

    public Double getLow() {
        return low;
    }

    public void setLow(Double low) {
        this.low = low;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public LocalDateTime getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(LocalDateTime priceDate) {
        this.priceDate = priceDate;
    }

    public Double getPreviousPrice() {
        return previousPrice;
    }

    public void setPreviousPrice(Double previousPrice) {
        this.previousPrice = previousPrice;
    }

    public LocalDateTime getPreviousPriceDate() {
        return previousPriceDate;
    }

    public void setPreviousPriceDate(LocalDateTime previousPriceDate) {
        this.previousPriceDate = previousPriceDate;
    }

    public Double getDiffRel() {
        return diffRel;
    }

    public void setDiffRel(Double diffRel) {
        this.diffRel = diffRel;
    }

    public Double getDiffAbs() {
        return diffAbs;
    }

    public void setDiffAbs(Double diffAbs) {
        this.diffAbs = diffAbs;
    }

    public InstrumentEntity getInstrument() {
        return instrument;
    }

    public void setInstrument(InstrumentEntity instrument) {
        this.instrument = instrument;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

}
