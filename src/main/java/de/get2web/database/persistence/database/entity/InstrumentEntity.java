package de.get2web.database.persistence.database.entity;


import de.get2web.database.model.enums.InstrumentType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Instrument entity data.
 */
@Entity
@Table(name = "bw_instrument")
public class InstrumentEntity implements Serializable {

    private static final long serialVersionUID = 2722895061440424174L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Instrument name.
     */
    private String name;

    /**
     * Instrument ISIN.
     */
    private String isin;

    /**
     * Instrument WKN.
     */
    private String wkn;

    /**
     * {@link InstrumentType}.
     */
    @Enumerated(EnumType.STRING)
    private InstrumentType type;

    /**
     * Instrument id external, third party integration instrument id.
     */
    private String idExternal;

    /**
     * Last trading day date time.
     */
    private LocalDateTime lastTradingDay;

    /**
     * Time stamp date time.
     */
    private LocalDateTime timestamp;

    /**
     * External sub type.
     */
    private String subType;

    /**
     * Instrument country name.
     */
    private String country;

    /**
     * Instrument origin name.
     */
    private String origin;

    /**
     * Instrument branch name.
     */
    private String branch;

    /**
     * Related {@link ContestEntity}.
     */
    @OneToOne
    @JoinColumn(name = "contest_id")
    private ContestEntity contest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getWkn() {
        return wkn;
    }

    public void setWkn(String wkn) {
        this.wkn = wkn;
    }

    public InstrumentType getType() {
        return type;
    }

    public void setType(InstrumentType type) {
        this.type = type;
    }

    public String getIdExternal() {
        return idExternal;
    }

    public void setIdExternal(String idExternal) {
        this.idExternal = idExternal;
    }

    public LocalDateTime getLastTradingDay() {
        return lastTradingDay;
    }

    public void setLastTradingDay(LocalDateTime lastTradingDay) {
        this.lastTradingDay = lastTradingDay;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public ContestEntity getContest() {
        return contest;
    }

    public void setContest(ContestEntity contest) {
        this.contest = contest;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    @Override
    public int hashCode() {
        final int prime = 51;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((idExternal == null) ? 0 : idExternal.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        InstrumentEntity other = (InstrumentEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (idExternal == null) {
            if (other.idExternal != null) {
                return false;
            }
        } else if (!idExternal.equals(other.idExternal)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InstrumentEntity [id=" + id + ", isin=" + isin + ", wkn=" + wkn + ", type=" + type + ", idExternal="
                + idExternal + ", subType=" + subType + ", contest=" + contest + "]";
    }

}
