package de.get2web.database.persistence.database.entity;


import de.get2web.database.model.enums.CorporateActionStatus;
import de.get2web.database.model.enums.CorporateActionType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Corporate action DB entity
 * Data related to third party payments data.
 */

@Entity
@Table(name = "bw_corporate_action")
public class CorporateActionEntity implements Serializable {

    private static final long serialVersionUID = -1950990153338030940L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link InstrumentEntity}.
     */
    @ManyToOne
    private InstrumentEntity instrument;

    /**
     * Date tame of execution.
     */
    private LocalDateTime date;

    /**
     * {@link CorporateActionType}
     */
    @Enumerated(EnumType.STRING)
    private CorporateActionType type;

    /**
     * {@link CorporateActionStatus}
     */
    @Enumerated(value = EnumType.STRING)
    private CorporateActionStatus status;

    /**
     * Die Bedeutung von Value ist je Corporate Action anders:
     * <p/>
     * CorporateAction         value
     * Split            split quota
     * Payout           payout
     * Dividend         stock quota
     * Coupon           coupon
     * Bonus stock      bonus stock quota
     */
    private Double value;

    /**
     * Bonus {@link InstrumentEntity} related.
     * TODO E.Chan, can you explain bonus stock.
     */
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "bonus_stock_id", nullable = true)
    private InstrumentEntity bonusStock;

    /**
     * Currency ISSO code.
     */
    private String isoCurrency;

    /**
     * Currency exchange rate value.
     */
    private Double exchangeRate;

    /**
     * TODO E.Chan, can you explain preemptiveRightPrice.
     */
    private Double preemptiveRightPrice;

    /**
     * Action related to portfolios ({@link PortfolioEntity}).
     */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "bw_corporate_action_portfolio", joinColumns = {@JoinColumn(name = "corporate_action_id")}, inverseJoinColumns = @JoinColumn(name = "portfolio_id"))
    private List<PortfolioEntity> portfolios;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InstrumentEntity getInstrument() {
        return instrument;
    }

    public void setInstrument(InstrumentEntity instrument) {
        this.instrument = instrument;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public CorporateActionType getType() {
        return type;
    }

    public void setType(CorporateActionType type) {
        this.type = type;
    }

    public CorporateActionStatus getStatus() {
        return status;
    }

    public void setStatus(CorporateActionStatus status) {
        this.status = status;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public InstrumentEntity getBonusStock() {
        return bonusStock;
    }

    public void setBonusStock(InstrumentEntity bonusStock) {
        this.bonusStock = bonusStock;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Double getPreemptiveRightPrice() {
        return preemptiveRightPrice;
    }

    public void setPreemptiveRightPrice(Double preemptiveRightPrice) {
        this.preemptiveRightPrice = preemptiveRightPrice;
    }

    public List<PortfolioEntity> getPortfolios() {
        return portfolios;
    }

    public void setPortfolios(List<PortfolioEntity> portfolios) {
        this.portfolios = portfolios;
    }

}
