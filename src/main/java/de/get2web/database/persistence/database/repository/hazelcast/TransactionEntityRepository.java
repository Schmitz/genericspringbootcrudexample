package de.get2web.database.persistence.database.repository.hazelcast;

import de.get2web.database.persistence.database.entity.TransactionEntity;
import org.springframework.data.hazelcast.repository.HazelcastRepository;

/**
 * Class TransactionEntityRepository
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO aschmitz describe Class
 * @created 2018-12-04
 * @since 1.0
 */

public interface TransactionEntityRepository extends HazelcastRepository<TransactionEntity, Long> {

}
