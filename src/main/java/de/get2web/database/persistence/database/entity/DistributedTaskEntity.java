package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Distributed task configuration data.
 */
@Entity
@Table(name = "bw_distributed_task")
public class DistributedTaskEntity implements Serializable {

    private static final long serialVersionUID = -2177695950276761588L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Task name.
     */
    private String task;

    /**
     * Host name.
     */
    private String hostName;

    /**
     * Environment.
     */
    private String env;

    /**
     * Short description.
     */
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
