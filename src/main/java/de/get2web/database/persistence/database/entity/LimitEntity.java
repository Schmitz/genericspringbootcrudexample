package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Limit entity.
 */
@Entity
@Table(name = "bw_limit")
public class LimitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link PortfolioEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "portfolio_id")
    private PortfolioEntity portfolio;

    /**
     * Related {@link InstrumentEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "instrument_id")
    private InstrumentEntity instrument;

    /**
     * Limit value data.
     */
    private Double limitValue;

    /**
     * ISSO currency key.
     */
    private String isoCurrency;

    /**
     * {@link LimitStatus}.
     */
    @Enumerated(EnumType.STRING)
    private LimitStatus status;

    /**
     * Creation date time.
     */
    private LocalDateTime created;

    /**
     * Last update date time.
     */
    private LocalDateTime lastUpdate;

    /**
     * Limit hit date time.
     */
    private LocalDateTime limitHit;

    /**
     * {@link LimitType}.
     */
    @Enumerated(EnumType.STRING)
    private LimitType type;

    /**
     * exchange where limit is to be checked
     */
    private String idExchangeExternal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InstrumentEntity getInstrument() {
        return instrument;
    }

    public void setInstrument(InstrumentEntity instrument) {
        this.instrument = instrument;
    }

    public Double getLimitValue() {
        return limitValue;
    }

    public void setLimitValue(Double limitValue) {
        this.limitValue = limitValue;
    }

    public PortfolioEntity getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(PortfolioEntity portfolio) {
        this.portfolio = portfolio;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public LimitStatus getStatus() {
        return status;
    }

    public void setStatus(LimitStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public LocalDateTime getLimitHit() {
        return limitHit;
    }

    public void setLimitHit(LocalDateTime limitHit) {
        this.limitHit = limitHit;
    }

    public LimitType getType() {
        return type;
    }

    public void setType(LimitType type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 41;
        int result = 1;
        result = prime * result + ((idExchangeExternal == null) ? 0 : idExchangeExternal.hashCode());
        result = prime * result + ((instrument == null) ? 0 : instrument.hashCode());
        result = prime * result + ((isoCurrency == null) ? 0 : isoCurrency.hashCode());
        result = prime * result + ((portfolio == null) ? 0 : portfolio.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LimitEntity other = (LimitEntity) obj;
        if (idExchangeExternal == null) {
            if (other.idExchangeExternal != null) {
                return false;
            }
        } else if (!idExchangeExternal.equals(other.idExchangeExternal)) {
            return false;
        }
        if (instrument == null) {
            if (other.instrument != null) {
                return false;
            }
        } else if (!instrument.equals(other.instrument)) {
            return false;
        }
        if (isoCurrency == null) {
            if (other.isoCurrency != null) {
                return false;
            }
        } else if (!isoCurrency.equals(other.isoCurrency)) {
            return false;
        }
        if (portfolio == null) {
            if (other.portfolio != null) {
                return false;
            }
        } else if (!portfolio.equals(other.portfolio)) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        return true;
    }

    public String getIdExchangeExternal() {
        return idExchangeExternal;
    }

    public void setIdExchangeExternal(String idExchangeExternal) {
        this.idExchangeExternal = idExchangeExternal;
    }

}
