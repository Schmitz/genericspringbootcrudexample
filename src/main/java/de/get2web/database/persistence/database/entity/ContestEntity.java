package de.get2web.database.persistence.database.entity;


import de.get2web.database.model.enums.DataProviderType;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Contest entity, data related to contests. Configuration data.
 */
@Entity
@Table(name = "bw_contest")
public class ContestEntity implements Serializable {

    private static final long serialVersionUID = -3172286028900756255L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link TenantEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "tenant_id")
    private TenantEntity tenant;

    /**
     * Starting date time.
     */
    @Column(name = "startDate")
    private LocalDateTime startDate;

    /**
     * End date time.
     */
    @Column(name = "endDate")
    private LocalDateTime endDate;

    /**
     * Registration start date time.
     */
    @Column(name = "startRegistrationDate")
    private LocalDateTime startRegistrationDate;

    /**
     * Registration end date time.
     */
    @Column(name = "endRegistrationDate")
    private LocalDateTime endRegistrationDate;

    /**
     * ISSO currency key.
     */
    @Column(name = "isoCurrency")
    private String isoCurrency;

    /**
     * Configured starting credit.
     */
    @Column(name = "startCredit")
    private Double startCredit;

    /**
     * Third party data provider {@link DataProviderType}.
     */
    @Column(name = "dataProvider")
    @Enumerated(EnumType.STRING)
    private DataProviderType dataProvider;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getStartRegistrationDate() {
        return startRegistrationDate;
    }

    public void setStartRegistrationDate(LocalDateTime startRegistrationDate) {
        this.startRegistrationDate = startRegistrationDate;
    }

    public LocalDateTime getEndRegistrationDate() {
        return endRegistrationDate;
    }

    public void setEndRegistrationDate(LocalDateTime endRegistrationDate) {
        this.endRegistrationDate = endRegistrationDate;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public Double getStartCredit() {
        return startCredit;
    }

    public void setStartCredit(Double startCredit) {
        this.startCredit = startCredit;
    }

    public TenantEntity getTenant() {
        return tenant;
    }

    public void setTenant(TenantEntity tenant) {
        this.tenant = tenant;
    }

    public DataProviderType getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(DataProviderType dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public int hashCode() {
        final int prime = 61;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ContestEntity other = (ContestEntity) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
