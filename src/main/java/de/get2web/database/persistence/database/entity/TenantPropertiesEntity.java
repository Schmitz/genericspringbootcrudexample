package de.get2web.database.persistence.database.entity;

import javax.persistence.*;

/**
 * Contest related static data properties.
 */
@Entity
@Table(name = "bw_dp_properties")
public class TenantPropertiesEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Related {@link ContestEntity}.
     */
    @OneToOne
    @JoinColumn(name = "contest_id")
    private ContestEntity contest;

    /**
     * Static data (file).
     */
    @Column(columnDefinition = "TEXT")
    private String properties;

    public ContestEntity getContest() {
        return contest;
    }

    public void setContest(ContestEntity contest) {
        this.contest = contest;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
