package de.get2web.database.persistence.database.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.io.Serializable;

/**
 * {@link ExchangeRateReferenceEntity} key data.
 */
@Embeddable
public class ExchangeRateReferencePK implements Serializable {

    private static final long serialVersionUID = 1089313000693722338L;

    /**
     * ISSO currency code, for exchange from.
     */
    @Column(length = 3)
    private String isoCurrencyFrom;

    /**
     * ISSO currency code, for exchange to.
     */
    @Column(length = 3)
    private String isoCurrencyTo;

    /**
     * {@link ContestEntity}
     */
    @OneToOne
    @JoinColumn(name = "contest_id")
    private ContestEntity contest;

    public ExchangeRateReferencePK(String isoCurrencyFrom, String isoCurrencyTo, ContestEntity contest) {
        super();
        this.isoCurrencyFrom = isoCurrencyFrom;
        this.isoCurrencyTo = isoCurrencyTo;
        this.contest = contest;
    }

    public ExchangeRateReferencePK() {
        super();
    }

    public String getIsoCurrencyFrom() {
        return isoCurrencyFrom;
    }

    public void setIsoCurrencyFrom(String isoCurrencyFrom) {
        this.isoCurrencyFrom = isoCurrencyFrom;
    }

    public String getIsoCurrencyTo() {
        return isoCurrencyTo;
    }

    public void setIsoCurrencyTo(String isoCurrencyTo) {
        this.isoCurrencyTo = isoCurrencyTo;
    }

    public ContestEntity getContest() {
        return contest;
    }

    public void setContest(ContestEntity contest) {
        this.contest = contest;
    }

    @Override
    public int hashCode() {
        final int prime = 19;
        int result = 1;
        result = prime * result + ((contest == null) ? 0 : contest.hashCode());
        result = prime * result + ((isoCurrencyFrom == null) ? 0 : isoCurrencyFrom.hashCode());
        result = prime * result + ((isoCurrencyTo == null) ? 0 : isoCurrencyTo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ExchangeRateReferencePK other = (ExchangeRateReferencePK) obj;
        if (contest == null) {
            if (other.contest != null) {
                return false;
            }
        } else if (!contest.equals(other.contest)) {
            return false;
        }
        if (isoCurrencyFrom == null) {
            if (other.isoCurrencyFrom != null) {
                return false;
            }
        } else if (!isoCurrencyFrom.equals(other.isoCurrencyFrom)) {
            return false;
        }
        if (isoCurrencyTo == null) {
            if (other.isoCurrencyTo != null) {
                return false;
            }
        } else if (!isoCurrencyTo.equals(other.isoCurrencyTo)) {
            return false;
        }
        return true;
    }
}
