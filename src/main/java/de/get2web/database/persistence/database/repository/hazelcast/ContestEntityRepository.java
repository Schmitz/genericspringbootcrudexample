package de.get2web.database.persistence.database.repository.hazelcast;

import de.get2web.database.persistence.database.entity.ContestEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.hazelcast.repository.HazelcastRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Class ContestEntityRepository
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO aschmitz describe Class
 * @created 2018-12-04
 * @since 1.0
 */

public interface ContestEntityRepository extends HazelcastRepository<ContestEntity, Long> {

    @Query(
            value = "SELECT * FROM bw_contest where tenant_id = :tenantId",
            countQuery = "SELECT count(*) FROM bw_contest where tenant_id = :tenantId",
            nativeQuery = true
    )
    Page<ContestEntity> findByTenantId(@PathVariable Long tenantId, Pageable pageable);

}
