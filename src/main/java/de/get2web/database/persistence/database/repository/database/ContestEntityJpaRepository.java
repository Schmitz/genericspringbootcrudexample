package de.get2web.database.persistence.database.repository.database;

import de.get2web.database.persistence.database.entity.ContestEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Class ContestEntityJpaRepository
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO aschmitz describe Class
 * @created 2018-12-03
 * @since 1.0
 */
public interface ContestEntityJpaRepository extends PagingAndSortingRepository<ContestEntity, Long> {

    /**
     * Query is for the hazelcast server
     *
     * @return
     */
    @Query("SELECT t.id FROM ContestEntity t")
    Iterable<Integer> findAllId();


}
