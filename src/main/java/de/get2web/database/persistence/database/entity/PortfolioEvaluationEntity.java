package de.get2web.database.persistence.database.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Portfolio evaluation, performance data information.
 */
@Entity
@Table(name = "bw_portfolio_evaluation")
public class PortfolioEvaluationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Data time, calculated.
     */
    private LocalDate date;

    /**
     * Related {@link PortfolioEntity}.
     */
    @ManyToOne
    @JoinColumn(name = "portfolio_id")
    private PortfolioEntity portfolio;

    /**
     * Current (live) calculated instrument value data.
     */
    private Double valueOfInstruments;

    /**
     * Current available credit (on disposal) data.
     */
    private Double availableCredit;

    /**
     * Current calculated instrument value of not executed orders (buy transactions, not selled yet).
     */
    private Double valueOfOpenOrders;

    /**
     * Total number of trades (transactions).
     */
    private Integer numberOfTrades;

    /**
     * Performance current value to start value, G/V gesamt in %
     * Percentage data.
     */
    private Double performanceRelTotal;

    /**
     * PerformanceAbsTotal (Gesamt G/V) = currentPortfolioValueTotal - currentLiquidityValueTotal
     * currentPortfolioValueTotal = currentPortfolioValue + paymentValue
     * currentLiquidityValueTotal  = valueAtActivationDate + paymentValue
     */
    private Double performanceAbsTotal;

    /**
     * G/V heute abs. = performanceRel = (performanceValueAbs / buy value)
     */
    private Double performanceRel;

    /**
     * G/V heute %, absolute performance.
     */
    private Double performanceAbs;

    /**
     * Posrtfolio info data computed, date time value.
     */
    private LocalDateTime computed;

    /**
     * Portfolio current total value (calculated on a fly). Equal to available
     * credit plus value of instruments.
     */
    private Double value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public PortfolioEntity getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(PortfolioEntity portfolio) {
        this.portfolio = portfolio;
    }

    public Double getValueOfInstruments() {
        return valueOfInstruments;
    }

    public void setValueOfInstruments(Double valueOfInstruments) {
        this.valueOfInstruments = valueOfInstruments;
    }

    public Double getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(Double availableCredit) {
        this.availableCredit = availableCredit;
    }

    public Double getValueOfOpenOrders() {
        return valueOfOpenOrders;
    }

    public void setValueOfOpenOrders(Double valueOfOpenOrders) {
        this.valueOfOpenOrders = valueOfOpenOrders;
    }

    public Integer getNumberOfTrades() {
        return numberOfTrades;
    }

    public void setNumberOfTrades(Integer numberOfTrades) {
        this.numberOfTrades = numberOfTrades;
    }

    public Double getPerformanceRelTotal() {
        return performanceRelTotal;
    }

    public void setPerformanceRelTotal(Double performanceRelTotal) {
        this.performanceRelTotal = performanceRelTotal;
    }

    public Double getPerformanceAbsTotal() {
        return performanceAbsTotal;
    }

    public void setPerformanceAbsTotal(Double performanceAbsTotal) {
        this.performanceAbsTotal = performanceAbsTotal;
    }

    public Double getPerformanceRel() {
        return performanceRel;
    }

    public void setPerformanceRel(Double performanceRel) {
        this.performanceRel = performanceRel;
    }

    public Double getPerformanceAbs() {
        return performanceAbs;
    }

    public void setPerformanceAbs(Double performanceAbs) {
        this.performanceAbs = performanceAbs;
    }

    public LocalDateTime getComputed() {
        return computed;
    }

    public void setComputed(LocalDateTime computed) {
        this.computed = computed;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

}
