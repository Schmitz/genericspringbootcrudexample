package de.get2web.database.persistence.database.entity;

/**
 * {@link Limit} status type.
 * ACTIVE - limit issued,
 * INACTIVE - limit is not active,
 * NOTIFIED - limit executed.
 */
public enum LimitStatus {

    ACTIVE, INACTIVE, NOTIFIED
}
