package de.get2web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * <h1>Class Application</h1>
 * <p>
 * SpringBoot Application set -Dspring.profiles.active
 * for the right Settings
 * see<br />
 * {@link de.get2web.config.ApplicationConfiguration}<br />
 * {@link de.get2web.config.HazelcastConfiguration}<br />
 * for the java config
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
@EnableCaching
@SpringBootApplication
public class Application {

    /**
     * Condition to load the Hazelcast Configuration or not String value of Boolean
     */
    public static final String HAZELCAST_ENABLED = "true";

    /**
     * run the Spring Boot Application
     *
     * @param args the application arguments
     * @throws Exception
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
