package de.get2web.config;

import com.hazelcast.config.*;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.web.WebFilter;
import de.get2web.Application;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;

/**
 * <h1>class HazelcastConfiguration</h1>
 * <p>
 * A conditional configuration that potentially adds the bean definitions in
 * this class to the Spring application context, depending on whether the
 * {@code @ConditionalOnExpression} is true or not.
 * </p>
 * <p>
 * When true, beans are added that create a Hazelcast instance, and bind this
 * instance to Tomcat for storage of HTTP sessions, instead of Tomcat's default
 * implementation.
 * </p>
 */
@Configuration
@PropertySource("classpath:application.properties")
@ConditionalOnExpression(Application.HAZELCAST_ENABLED)
public class HazelcastConfiguration {

    /**
     * cluser memebers for connecting between the hazelcast instances
     */
    @Value("${hazelcast.cluster.members}")
    List<String> clusterMembers;

    /**
     * <h2>config()</h2>
     * <p>
     * Create a Hazelcast {@code Config} object as a bean. Spring Boot will use
     * the presence of this to determine that a {@code HazelcastInstance} should
     * be created with this configuration.
     * </p>
     * <p>
     * As a simple side-step to possible networking issues, turn off multicast
     * in favour of TCP connection to the local host.
     * </p>
     *
     * @return Configuration for the Hazelcast instance
     */
    @Bean
    public Config config() {
        Config config = new Config();
        JoinConfig joinConfig = config.getNetworkConfig().getJoin();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getTcpIpConfig().setEnabled(true).setMembers(this.clusterMembers);
        MapConfig mapConfig = new MapConfig();
        mapConfig.setName("EntityServiceCache");
        mapConfig.setMaxSizeConfig(new MaxSizeConfig(200, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE));
        mapConfig.setEvictionPolicy(EvictionPolicy.LFU);
        mapConfig.setTimeToLiveSeconds(86400);
        config.addMapConfig(mapConfig);

        return config;
    }

    /**
     * <h2>webFilter(HazelcastInstance hazelcastInstance)</h2>
     * <p>
     * Create a web filter. Parameterize this with two properties,
     * <ol>
     * <li><i>instance-name</i>
     * Direct the web filter to use the existing Hazelcast instance rather than
     * to create a new one.</li>
     * <li><i>sticky-session</i>
     * As the HTTP session will be accessed from multiple processes, deactivate
     * the optimization that assumes each user's traffic is routed to the same
     * process for that user.</li>
     * </ol>
     * </p>
     * <p>
     * Spring will assume dispatcher types of {@code FORWARD}, {@code INCLUDE}
     * and {@code REQUEST}, and a context pattern of "{@code /*}".
     * </p>
     *
     * @param hazelcastInstance Created by Spring
     * @return The web filter for Tomcat
     */
    @Bean
    public WebFilter webFilter(HazelcastInstance hazelcastInstance) {
        Properties properties = new Properties();
        properties.put("instance-name", hazelcastInstance.getName());
        properties.put("sticky-session", "false");
        return new WebFilter(properties);
    }

    /**
     * <h2>dataSource()</h2>
     * <p>
     * Use SpringBoot {@link DataSourceBuilder}
     * to create A DataSource Bean with the given
     * 'spring.datasource' properties this is required to use the HazelcastRepositories
     * </p>
     *
     * @return A datasource onto the database server
     */
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }
}
