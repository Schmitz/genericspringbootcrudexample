package de.get2web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <h1>Class ApplicationConfiguration</h1>
 * <p>The main java config for the SpringBootApplication</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-12-04
 * @since 1.0
 */
@Configuration
@EnableWebMvc
@EnableSwagger2
public class ApplicationConfiguration implements WebMvcConfigurer {

    /**
     * <h2>Bean PropertySourcesPlaceholderConfigurer</h2>
     * <p>This Bean is need for list conversion of the application properties file. So than it is possible to use:</p>
     * <pre>
     *     @Value("${list}")
     *     private List<String> list;
     * </pre>
     * <p>in your Class.</p>
     * <p>Without DefaultConversionService, you can only take comma separated String into String array when you inject the value into your
     * field, but DefaultConversionService does a few convenient magic for you and will add those into Collection, Array, etc.
     * ( check the implementation if you'd like to know more about it )</p>
     *
     * @return {@link PropertySourcesPlaceholderConfigurer}
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * <h2>Bean ConversionService</h2>
     * <p>See PropertySourcesPlaceholderConfigurer for more explanation</p>
     *
     * @return {@link ConversionService}
     */
    @Bean
    public ConversionService conversionService() {
        return new DefaultConversionService();
    }

    /**
     * <h2>Add Resource Handler</h2>
     * <p>required for swagger-ui</p>
     * <p>see https://swagger.io/tools/swagger-ui/</p>
     *
     * @param registry {@link ResourceHandlerRegistry}
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * <h2>Swagger Docket</h2>
     * <p>create and configure a swagger Docket</p>
     *
     * @return {@link Docket}
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("de.get2web.rest"))
                .paths(PathSelectors.any())
                .build();
    }

}
