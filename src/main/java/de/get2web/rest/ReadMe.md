# rest package
[ControllerDiagram]: ../../../../../../src/etc/readme-img/ControllerDiagram.png "ControllerDiagram.png"
[RestServiceDiagram]: ../../../../../../src/etc/readme-img/RestServiceDiagram.png "RestServiceDiagram.png"

This document describes the Architecture of the rest package. We have a package v1 which means Version 1. You can add a
directory v2 for Version 2 so the application stays compatible with older clients.<br />
I would suggest to save the information in a request statistic entity. Then we know how many clients are using which version.
Based on this Information it would be possible to remove a version in knowing of no usage.

### RestController or the JSON REST API Layer
The RestControllers are the Rest Api Layer which are providing RESTFull Services for clients. The Service-URL Specification follows mostly
the OpenApi Specification. see https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md#pathsObject 
<br />
This means we have the following url structure:
```
GET         /DTO-NAME/              -- returns a Page Object with DTOs
GET         /DTO-NAME/list/{id}     -- returns a Page Object with DTOs contest by tenant.id for example
GET         /DTO-NAME/{ID}          -- returns DTO for the ID
POST | PUT  /DTO-NAME/              -- assuming RequestBody is a DTO it saves or updates the Object
DELETE      /DTO-NAME/{ID}          -- deletes a object by id
```
In the rest.v1.controller package is a BaseRestController class which Autowires a implementation of the BaseRestService
with the common function. So extending the BaseController requires some other classes.<br />
This is described later in the section 'HowTo: Create a new Domain Object from Rest to DB'<br /><br />
In this Diagram you can see the inheritance of the BaseRestController. The DomainControllers are empty because they use the 
basic implementation methods from the BaseRestController. With this Design Pattern it reduces massive duplicated code but 
it is possible to override each super class function.<br />
![Image of ControllerDiagram][ControllerDiagram]<br /><br />
For Example the ContestRestController provides the RequestMapping for the ContestDTO and extends The BaseRestController. 
```
@RestController
@RequestMapping(value = "/v1/contest")
public class ContestRestController extends BaseRestController<ContestDTO> {
    // override methods if required see BaseRestController
    // add additional service urls if required
}
```  
### RestService Layer
The RestService Layer provides the crud service Implementations for each DTO. 
It maps a DTO to Entity and the other way around. Uses the CrudService for the specific DTO<br />
also the package de.get2web.rest.v1.service has a BaseRestService but it is not a class like the controller, it is a interface<br />
So we have custom Implementations for each DTO. With that Design pattern it is possible to<br />
Autowire a Implementation of the `BaseRestService<T> service` Spring detects which Implementation has to Autowired by the
Service Implementation of T<br />
<br />
The Diagram shows the inheritance of the RestService Layer<br />
![Image of RestServiceDiagram][RestServiceDiagram]