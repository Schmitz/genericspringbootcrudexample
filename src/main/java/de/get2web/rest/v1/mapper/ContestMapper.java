package de.get2web.rest.v1.mapper;

import de.get2web.database.persistence.database.entity.ContestEntity;
import de.get2web.rest.v1.model.ContestDTO;
import org.springframework.stereotype.Component;

/**
 * <h1>Class ContestMapper</h1>
 * <p>subclass of BaseModelMapper for mapping the {@link ContestEntity} and {@link ContestDTO}</p>
 * <p>see {@link BaseModelMapper} for more details</p>
 * <p>if required the methods from super can be overwritten</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-12-04
 * @since 1.0
 */
@Component
public class ContestMapper extends BaseModelMapper<ContestEntity, ContestDTO> {
    // override super methods
    // add custom methods
}
