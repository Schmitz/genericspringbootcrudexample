package de.get2web.rest.v1.mapper;

import de.get2web.database.persistence.database.entity.TransactionEntity;
import de.get2web.rest.v1.model.TransactionDTO;
import org.springframework.stereotype.Component;

/**
 * <h1>Class ContestMapper</h1>
 * <p>subclass of BaseModelMapper for mapping the {@link TransactionEntity} and {@link TransactionDTO}</p>
 * <p>see {@link BaseModelMapper} for more details</p>
 * <p>if required the methods from super can be overwritten</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-12-04
 * @since 1.0
 */
@Component
public class TransactionMapper extends BaseModelMapper<TransactionEntity, TransactionDTO> {

}
