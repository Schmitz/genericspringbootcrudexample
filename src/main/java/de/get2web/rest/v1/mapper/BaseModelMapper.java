package de.get2web.rest.v1.mapper;

import org.modelmapper.ModelMapper;

/**
 * <h1>Class BaseModelMapper</h1>
 * <p>
 * generic Base Implementation for mapping DTO to Entity objects or the other way around. assume the Objects having
 * the same member.<br />
 * can be overwritten in its subclasses
 * </p>
 * <p>
 * it uses the maven-module:<br />
 * <pre>
 * <dependency>
 * <groupId>org.modelmapper</groupId>
 * <artifactId>modelmapper</artifactId>
 * <version>${modelmapper.version}</version>
 * </dependency>
 * </pre>
 * </p>
 * <p>
 * see http://modelmapper.org/
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-30
 * @since 1.0
 */

public class BaseModelMapper<T, D> {

    public T mapFromDTO(T entityObject, D dtoObject) {
        ModelMapper modelMapper = new ModelMapper();
        return (T) modelMapper.map(dtoObject, entityObject.getClass());
    }

    public D mapFromEntity(T entityObject, D dtoObject) {
        ModelMapper modelMapper = new ModelMapper();
        return (D) modelMapper.map(entityObject, dtoObject.getClass());
    }

}
