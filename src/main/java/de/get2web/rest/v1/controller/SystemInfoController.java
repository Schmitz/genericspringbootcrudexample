package de.get2web.rest.v1.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * Class SystemInfoController
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO aschmitz describe Class
 * @created 2018-12-07
 * @since 1.0
 */
@RestController
@RequestMapping(value = "/v1/system-info")
public class SystemInfoController {


    @RequestMapping(value = "/", method = {RequestMethod.GET})
    public ResponseEntity getSystemInfo(HttpSession session) {

        return new ResponseEntity<>("", HttpStatus.OK);
    }


}
