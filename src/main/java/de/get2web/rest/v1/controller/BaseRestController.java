package de.get2web.rest.v1.controller;

import de.get2web.rest.v1.service.BaseRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * <h1>Class BaseRestController</h1>
 * <p>
 * This class provides Generic Basic Crud Service URLs.<br />
 * generic T is a DTO Object which should be a direct representation of an EntityObject<br />
 * with this generic inheritance we have a construct with basic implementation for all CRUD Operations. Which can all be overwritten
 * to implement custom methods or to manipulate the DTO before sending
 * <br />
 * requires other implementations:
 * </p>
 * <ol>
 * <li>a DTO model, see {@link de.get2web.rest.v1.model.ContestDTO} for example</li>
 * <li>an Implementation of the BaseRestService Interface, see {@link de.get2web.rest.v1.service.ContestRestService} for example</li>
 * <li>BaseRestService requires a sub class of BaseModelMapper, see {@link de.get2web.rest.v1.mapper.ContestMapper} for example</li>
 * </ol>
 * <p>
 * The following api-urls are provided by using this as superclass<br />
 * '/subclass-mapping' (the @RequestMapping annotation from subclass) +
 * </p>
 * <ol>
 * <li>/ - GET returns a {@link org.springframework.data.domain.PageImpl}</li>
 * <li>/list/{id} - GET returns a {@link org.springframework.data.domain.PageImpl}</li>
 * <li>/{id} - GET returns a single DTO</li>
 * <li>/ - POST/PUT with RequestBody containing DTO returns the saved DTO</li>
 * <li>/{id} - DELETE returns a simple Boolean</li>
 * </ol>
 * <p>
 * The url mapping /list/{id} could be implementated to get Contests by Tenant.id for example
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO: <ol>
 * <li>DELETE should not be a simple Boolean better a BaseDTO object</li>
 * <li>In error cases it should return a BaseDTO object (maybe)</li>
 * </ol>
 * @created 2018-11-29
 * @since 1.0
 */
public class BaseRestController<T> {

    @Autowired
    private BaseRestService<T> service;

    /**
     * <h2>getList(int page,int size)</h2>
     * <p>GET: /?page=int&size=int</p>
     * <p>
     * There is no list function without paging params. To get all it is simple to use:<br />
     * page=0&size=Integer.MAX_VALUE or Long.MAX_VALUE
     * </p>
     *
     * @param page int the page to show starts with 0
     * @param size int the number of page entries
     * @return Page\<T\>
     * @TODO optional sort params
     */
    @RequestMapping(value = "/", params = {"page", "size"}, method = {RequestMethod.GET})
    public ResponseEntity getList(@RequestParam("page") int page,
                                  @RequestParam("size") int size) {
        Pageable pageable = PageRequest.of(page, size);
        return new ResponseEntity<>(this.service.list(pageable), HttpStatus.OK);
    }

    /**
     * <h2>getListWithId(int page, int size)</h2>
     * <p>GET: /list/{id}/?page=int&size=int</p>
     * <p>list by foreign object id for example. this can be implemented in BaseRestService Implementation</p>
     *
     * @param page int the page to show starts with 0
     * @param size int the number of page entries
     * @return Page\<T\>
     * @TODO optional sort params
     */
    @RequestMapping(value = "/list/{id}", params = {"page", "size"}, method = {RequestMethod.GET})
    public ResponseEntity getListWithId(@RequestParam("page") int page,
                                        @RequestParam("size") int size,
                                        @PathVariable Long id) {
        Pageable pageable = PageRequest.of(page, size);
        return new ResponseEntity<>(this.service.listWithId(id, pageable), HttpStatus.OK);
    }

    /**
     * <h2>getOne(Long id)</h2>
     * <p>GET: /{id}</p>
     * <p>get One DTO Object</p>
     *
     * @param id Long id of the DTO/Entity
     * @return T
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.GET})
    public ResponseEntity getOne(@PathVariable Long id) {
        return new ResponseEntity<>(this.service.getById(id), HttpStatus.OK);
    }

    /**
     * <h2>save(T dtoObject)</h2>
     * <p>POST,PUT: /</p>
     * <p>save or update a dtoObject</p>
     *
     * @param @RequestBody dtoObject the DTO Object to be saved
     * @return T
     */
    @RequestMapping(value = "/", method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity save(@RequestBody T dtoObject) {
        return new ResponseEntity<>(this.service.save(dtoObject), HttpStatus.OK);
    }

    /**
     * <h2>delete(Long id)</h2>
     * <p>DELETE: /{id}</p>
     * <p>deletes an data entrie with the given id</p>
     *
     * @param id Long date entry id
     * @return
     */
    @RequestMapping(value = "/{id}", method = {RequestMethod.DELETE})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        return new ResponseEntity<>(this.service.delete(id), HttpStatus.OK);
    }


}
