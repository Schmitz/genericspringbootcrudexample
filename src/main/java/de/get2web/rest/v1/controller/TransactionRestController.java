package de.get2web.rest.v1.controller;

import de.get2web.rest.v1.model.TransactionDTO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <h1>Class TransactionRestController</h1>
 * <p/>
 * <p>provides all Rest Urls for the Domain Object {@link TransactionDTO}</p>
 * <p>See {@link BaseRestController} for the usual base implementation for:</p>
 * <ol>
 * <li>GET list {domainObject}/</li>
 * <li>GET listWithId {domainObject}/list/id/</li>
 * <li>GET one {domainObject}/id</li>
 * <li>POST/PUT save {domainObject}/</li>
 * <li>DELETE delete {domainObject}/id</li>
 * </ol>
 * <p>If necessary each function can be overwritten here or custom methods can be implemented</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
@RestController
@RequestMapping(value = "/v1/transaction")
public class TransactionRestController extends BaseRestController<TransactionDTO> {
    // override methods if required see BaseRestController
    // add additional service urls if required
}
