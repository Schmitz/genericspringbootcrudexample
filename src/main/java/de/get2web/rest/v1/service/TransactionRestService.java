package de.get2web.rest.v1.service;

import de.get2web.database.persistence.database.entity.TransactionEntity;
import de.get2web.database.service.TransactionEntityService;
import de.get2web.rest.v1.mapper.TransactionMapper;
import de.get2web.rest.v1.model.TransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <h1>Class TransactionRestService</h1>
 * <p>
 * implementation of the interface BaseRestService by using the {@link TransactionDTO} as generic.
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @uses TransactionEntityService
 * @uses TransactionMapper
 * @created 2018-11-30
 * @since 1.0
 */
@Service
public class TransactionRestService implements BaseRestService<TransactionDTO> {


    @Autowired
    private TransactionEntityService transactionEntityService;

    @Autowired
    private TransactionMapper transactionMapper;

    /**
     * <h2>save(TransactionDTO transactionDTO)</h2>
     * <p>
     * map the DTO to an Entity object and save the entity Object in the database.<br />
     * implemented from interface
     * </p>
     * {@inheritDoc}
     *
     * @param transactionDTO {@link TransactionDTO} Database Entity Object
     * @return {@link TransactionDTO}
     */
    @Override
    public TransactionDTO save(TransactionDTO transactionDTO) {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity = this.transactionMapper.mapFromDTO(transactionEntity, transactionDTO);
        transactionEntity = this.transactionEntityService.save(transactionEntity);
        if (transactionEntity.getId() == null) {
            transactionDTO.setSuccess(Boolean.FALSE);
            transactionDTO.setMsgKey("err.contest.not.saved");
        } else {
            transactionDTO = this.transactionMapper.mapFromEntity(transactionEntity, transactionDTO);
            transactionDTO.setSuccess(Boolean.TRUE);
        }
        return transactionDTO;
    }

    /**
     * <h2>getById(Long id)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param id Long id
     * @return {@link TransactionDTO}
     */
    @Override
    public TransactionDTO getById(Long id) {
        Optional<TransactionEntity> transactionEntity = this.transactionEntityService.getById(id);
        if (!transactionEntity.isPresent()) {
            TransactionDTO transactionDTO = new TransactionDTO();
            transactionDTO.setSuccess(Boolean.FALSE);
            transactionDTO.setMsgKey("err.transaction.not.found");
        }
        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO = this.transactionMapper.mapFromEntity(transactionEntity.get(), transactionDTO);
        transactionDTO.setSuccess(Boolean.TRUE);
        return transactionDTO;
    }

    /**
     * <h2>list(Pageable pageable)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param pageable {@link Pageable}
     * @return {@link Page} Page<TransactionDTO>
     */
    @Override
    public Page<TransactionDTO> list(Pageable pageable) {
        return this._pageTransactionDTO(this.transactionEntityService.list(pageable), pageable);
    }

    /**
     * <h2>listWithId(Long id, Pageable pageable)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param id       Long id
     * @param pageable {@link Pageable}
     * @return {@link Page} Page<TransactionDTO>
     */
    @Override
    public Page<TransactionDTO> listWithId(Long id, Pageable pageable) {
        return null;
    }

    /**
     * <h2>delete(Long id)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param id Long Id
     * @return {@link Boolean}
     */
    @Override
    public Boolean delete(Long id) {
        return this.transactionEntityService.delete(id);
    }

    /**
     * <h2>_pageTransactionDTO(Page<TransactionEntity> transactionEntities, Pageable pageable)</h2>
     * <p>maps each object of an EntityList to a DTO and put to an DTOList</p>
     *
     * @param contestEntities Page<TransactionEntity>
     * @param pageable        {@link Pageable}
     * @return
     */
    private Page<TransactionDTO> _pageTransactionDTO(Page<TransactionEntity> transactionEntities, Pageable pageable) {
        List<TransactionDTO> transactionDTOList = new ArrayList<>();
        transactionEntities.getContent().forEach(item -> transactionDTOList.add(this.transactionMapper.mapFromEntity(item, new TransactionDTO())));
        return new PageImpl<>(transactionDTOList, pageable, transactionEntities.getTotalElements());
    }
}
