package de.get2web.rest.v1.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * <h1>Class BaseRestService</h1>
 * <p>
 * a generic RestService interface ot use different implementations in the BaseRestController.<br />
 * An implementation of this interface is AutoWired in the BaseController
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-30
 * @since 1.0
 */
public interface BaseRestService<T> {

    /**
     * <h3>save(T t)</h3>
     * <p>
     * save a DTO Object
     * </p>
     *
     * @param t T DTO object
     * @return T DTO object
     */
    T save(T t);

    /**
     * <h3>getById(Long id)</h3>
     * <p>get a DTO by Entity.id</p>
     *
     * @param id Long Entity.id
     * @return
     */
    T getById(Long id);

    /**
     * <h3>list(Pageable pageable)</h3>
     * <p>
     * returns a Page\<T\> with the DTOs
     * </p>
     *
     * @param {@link Pageable}
     * @return Page<T>
     */
    Page<T> list(Pageable pageable);

    /**
     * <h3>listWithId(Long id, Pageable pageable)</h3>
     * <p>
     * returns a Page\<T\> with the DTOs
     * </p>
     * <p>for data objects where we have more than one entry with an id
     * for example: all contests by tenant.id</p>
     *
     * @param id       Long
     * @param pageable {@link Pageable}
     * @return Page<T>
     */
    Page<T> listWithId(Long id, Pageable pageable);

    /**
     * <h3>delete</h3>
     * <p>
     * deletes an data entrie by id
     * </p>
     *
     * @param id
     * @return
     */
    Boolean delete(Long id);

}
