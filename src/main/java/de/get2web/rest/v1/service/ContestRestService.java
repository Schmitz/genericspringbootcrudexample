package de.get2web.rest.v1.service;

import de.get2web.database.persistence.database.entity.ContestEntity;
import de.get2web.database.service.ContestEntityService;
import de.get2web.rest.v1.mapper.ContestMapper;
import de.get2web.rest.v1.model.ContestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <h1>Class ContestRestService</h1>
 * <p>
 * implementation of the interface BaseRestService by using the {@link ContestDTO} as generic.
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @uses ContestEntityService
 * @uses ContestMapper
 * @created 2018-11-30
 * @since 1.0
 */
@Service
public class ContestRestService implements BaseRestService<ContestDTO> {


    @Autowired
    private ContestEntityService contestEntityService;

    @Autowired
    private ContestMapper contestMapper;

    /**
     * <h2>save(ContestDTO contestDTO)</h2>
     * <p>
     * map the DTO to an Entity object and save the entity Object in the database.<br />
     * implemented from interface
     * </p>
     * {@inheritDoc}
     *
     * @param contestDTO {@link ContestDTO} Database Entity Object
     * @return {@link ContestDTO}
     */
    @Override
    public ContestDTO save(ContestDTO contestDTO) {
        ContestEntity contestEntity = new ContestEntity();
        contestEntity = this.contestMapper.mapFromDTO(contestEntity, contestDTO);
        contestEntity = this.contestEntityService.save(contestEntity);
        if (contestEntity.getId() == null) {
            contestDTO.setSuccess(Boolean.FALSE);
            contestDTO.setMsgKey("err.contest.not.saved");
        } else {
            contestDTO = this.contestMapper.mapFromEntity(contestEntity, contestDTO);
            contestDTO.setSuccess(Boolean.TRUE);
        }
        return contestDTO;
    }

    /**
     * <h2>getById(Long id)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param id Long id
     * @return {@link ContestDTO}
     */
    @Override
    public ContestDTO getById(Long id) {
        Optional<ContestEntity> contestEntity = this.contestEntityService.getById(id);
        if (!contestEntity.isPresent()) {
            ContestDTO dto = new ContestDTO();
            dto.setSuccess(Boolean.FALSE);
            dto.setMsgKey("err.contest.not.found");
            return dto;
        }
        ContestDTO contestDTO = new ContestDTO();
        contestDTO = this.contestMapper.mapFromEntity(contestEntity.get(), contestDTO);
        contestDTO.setSuccess(Boolean.TRUE);
        return contestDTO;
    }

    /**
     * <h2>list(Pageable pageable)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param pageable {@link Pageable}
     * @return {@link Page} Page<ContestDTO>
     */
    @Override
    public Page<ContestDTO> list(Pageable pageable) {
        return this._pageContestDTO(this.contestEntityService.list(pageable), pageable);
    }

    /**
     * <h2>listWithId(Long id, Pageable pageable)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param id       Long id
     * @param pageable {@link Pageable}
     * @return {@link Page} Page<ContestDTO>
     */
    @Override
    public Page<ContestDTO> listWithId(Long id, Pageable pageable) {
        return this._pageContestDTO(this.contestEntityService.listWithId(id, pageable), pageable);
    }

    /**
     * <h2>delete(Long id)</h2>
     * <p></p>
     * {@inheritDoc}
     *
     * @param id Long Id
     * @return {@link Boolean}
     */
    @Override
    public Boolean delete(Long id) {
        return null;
    }

    /**
     * <h2>_pageContestDTO(Page<ContestEntity> contestEntities, Pageable pageable)</h2>
     * <p>maps each object of an EntityList to a DTO and put to an DTOList</p>
     *
     * @param contestEntities Page<ContestEntity>
     * @param pageable        {@link Pageable}
     * @return
     */
    private Page<ContestDTO> _pageContestDTO(Page<ContestEntity> contestEntities, Pageable pageable) {
        List<ContestDTO> contestDTOList = new ArrayList<>();
        contestEntities.getContent().forEach(item -> contestDTOList.add(this.contestMapper.mapFromEntity(item, new ContestDTO())));
        return new PageImpl<>(contestDTOList, pageable, contestEntities.getTotalElements());
    }

}
