package de.get2web.rest.v1.util;

/**
 * <h1>Class WsUtil</h1>
 * <p>
 * class for static webservice helper functions
 * </p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
@SuppressWarnings(value = "unused")
public class WsUtil {

    // add static methods


}
