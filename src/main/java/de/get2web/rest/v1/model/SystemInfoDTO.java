package de.get2web.rest.v1.model;

/**
 * Class SystemInfoDTO
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @TODO aschmitz describe Class
 * @created 2018-12-07
 * @since 1.0
 */

public class SystemInfoDTO {

    private String sessionId;

    private Long sessionCreationTime;

    private Long sessionLastAccessTime;


}
