package de.get2web.rest.v1.model;

import de.get2web.database.model.enums.PriceUnit;
import de.get2web.database.model.enums.TransactionType;
import de.get2web.database.persistence.database.entity.CorporateActionEntity;
import de.get2web.database.persistence.database.entity.InstrumentEntity;
import de.get2web.database.persistence.database.entity.PortfolioEntity;

import java.time.LocalDateTime;

/**
 * <h1>Class TransactionDTO</h1>
 * <p>a simple pojo representation for the Entity class {@link de.get2web.database.persistence.database.entity.TransactionEntity}</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
@SuppressWarnings(value = "unused")
public class TransactionDTO extends BaseDTO {

    private Long id;

    private PortfolioEntity portfolio;

    private InstrumentEntity instrument;

    private String idExchangeExternal;

    private CorporateActionEntity corporateAction;

    private TransactionType transactionType;

    private LocalDateTime executionDate = LocalDateTime.now();

    private String isoCurrency;

    private Double exchangeRate;

    private LocalDateTime exchangeRateDate = LocalDateTime.now();

    private PriceUnit priceUnit;

    private Double quantity;

    private LocalDateTime timestamp = LocalDateTime.now();

    private Double price;

    private LocalDateTime priceDate;

    private Double transactionCost;

    private Double accruedInterest;

    private String codeContributor;

    private String codeExchange;

    private Boolean editable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PortfolioEntity getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(PortfolioEntity portfolio) {
        this.portfolio = portfolio;
    }

    public InstrumentEntity getInstrument() {
        return instrument;
    }

    public void setInstrument(InstrumentEntity instrument) {
        this.instrument = instrument;
    }

    public String getIdExchangeExternal() {
        return idExchangeExternal;
    }

    public void setIdExchangeExternal(String idExchangeExternal) {
        this.idExchangeExternal = idExchangeExternal;
    }

    public CorporateActionEntity getCorporateAction() {
        return corporateAction;
    }

    public void setCorporateAction(CorporateActionEntity corporateAction) {
        this.corporateAction = corporateAction;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public LocalDateTime getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(LocalDateTime executionDate) {
        this.executionDate = executionDate;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public LocalDateTime getExchangeRateDate() {
        return exchangeRateDate;
    }

    public void setExchangeRateDate(LocalDateTime exchangeRateDate) {
        this.exchangeRateDate = exchangeRateDate;
    }

    public PriceUnit getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(PriceUnit priceUnit) {
        this.priceUnit = priceUnit;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public LocalDateTime getPriceDate() {
        return priceDate;
    }

    public void setPriceDate(LocalDateTime priceDate) {
        this.priceDate = priceDate;
    }

    public Double getTransactionCost() {
        return transactionCost;
    }

    public void setTransactionCost(Double transactionCost) {
        this.transactionCost = transactionCost;
    }

    public Double getAccruedInterest() {
        return accruedInterest;
    }

    public void setAccruedInterest(Double accruedInterest) {
        this.accruedInterest = accruedInterest;
    }

    public String getCodeContributor() {
        return codeContributor;
    }

    public void setCodeContributor(String codeContributor) {
        this.codeContributor = codeContributor;
    }

    public String getCodeExchange() {
        return codeExchange;
    }

    public void setCodeExchange(String codeExchange) {
        this.codeExchange = codeExchange;
    }

    public Boolean getEditable() {
        return editable;
    }

    public void setEditable(Boolean editable) {
        this.editable = editable;
    }
}
