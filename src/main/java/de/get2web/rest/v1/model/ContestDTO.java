package de.get2web.rest.v1.model;

import de.get2web.database.model.enums.DataProviderType;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <h1>Class ContestDTO</h1>
 * <p>a simple pojo representation for the Entity class {@link de.get2web.database.persistence.database.entity.ContestEntity}</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
@SuppressWarnings(value = "unused")
public class ContestDTO extends BaseDTO implements Serializable {

    private Long id;

    private TenantDTO tenant;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private LocalDateTime startRegistrationDate;

    private LocalDateTime endRegistrationDate;

    private String isoCurrency;

    private Double startCredit;

    private DataProviderType dataProvider;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TenantDTO getTenant() {
        return tenant;
    }

    public void setTenant(TenantDTO tenant) {
        this.tenant = tenant;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public LocalDateTime getStartRegistrationDate() {
        return startRegistrationDate;
    }

    public void setStartRegistrationDate(LocalDateTime startRegistrationDate) {
        this.startRegistrationDate = startRegistrationDate;
    }

    public LocalDateTime getEndRegistrationDate() {
        return endRegistrationDate;
    }

    public void setEndRegistrationDate(LocalDateTime endRegistrationDate) {
        this.endRegistrationDate = endRegistrationDate;
    }

    public String getIsoCurrency() {
        return isoCurrency;
    }

    public void setIsoCurrency(String isoCurrency) {
        this.isoCurrency = isoCurrency;
    }

    public Double getStartCredit() {
        return startCredit;
    }

    public void setStartCredit(Double startCredit) {
        this.startCredit = startCredit;
    }

    public DataProviderType getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(DataProviderType dataProvider) {
        this.dataProvider = dataProvider;
    }
}
