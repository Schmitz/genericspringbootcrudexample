package de.get2web.rest.v1.model;

import java.io.Serializable;

/**
 * <h1>Class TenantDTO</h1>
 * <p>a simple pojo representation for the Entity class {@link de.get2web.database.persistence.database.entity.TenantEntity}</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
@SuppressWarnings(value = "unused")
public class TenantDTO extends BaseDTO implements Serializable {

    private Long id;

    private String name;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
