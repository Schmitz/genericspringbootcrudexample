package de.get2web.rest.v1.model;

import java.io.Serializable;

/**
 * <h1>Class BaseDTO</h1>
 * <p>simple Base pojo to add the fields success and msg key to DTO subclasses</p>
 *
 * @author Axel Schmitz <axelschmitz@outlook.de>
 * @created 2018-11-29
 * @since 1.0
 */
public class BaseDTO implements Serializable {

    private Boolean success;

    private String msgKey;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsgKey() {
        return msgKey;
    }

    public void setMsgKey(String msgKey) {
        this.msgKey = msgKey;
    }
}
